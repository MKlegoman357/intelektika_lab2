import numpy as np
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.neural_network import MLPClassifier

data_file_name = "weatherAUS_normalized.csv"

weather_data = pd.read_csv(data_file_name)

weather_data_inputs = weather_data.drop(["RainTomorrow"], axis=1)
# weather_data_outputs = pd.get_dummies(weather_data["RainTomorrow"])
weather_data_outputs = weather_data["RainTomorrow"].map({"Yes": 1, "No": 0})

print("inputs:")
print(weather_data_inputs.head())

print("outputs:")
print(weather_data_outputs.head())

kf = KFold(n_splits=10)
clf = MLPClassifier(
    solver="sgd",
    activation="logistic",
    hidden_layer_sizes=(),
    learning_rate_init=0.1,
    # learning_rate="adaptive",
    momentum=0.5,
    # random_state=0,
    verbose=False,
)

scores = []

for train, test in kf.split(weather_data_inputs):
    weather_data_inputs_train, weather_data_outputs_train = \
        weather_data_inputs.iloc[train], weather_data_outputs.iloc[train]
    weather_data_inputs_test, weather_data_outputs_test = \
        weather_data_inputs.iloc[test], weather_data_outputs.iloc[test]

    print("fitting...")
    clf.fit(weather_data_inputs_train, weather_data_outputs_train)

    split_score = clf.score(weather_data_inputs_test, weather_data_outputs_test)

    scores.append(split_score)
    print(split_score)

average_score = np.average(scores)

print("average score: ", average_score)
