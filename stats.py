import statistics

import numpy as np


class StatsAttribute:
    def __init__(self, key, data_key, name=None, type="number", unique=False, ignore=False):
        self.key = key
        self.data_key = data_key
        self.name = data_key if name is None else name
        self.type = type
        self.unique = unique
        self.ignore = ignore


class AttributeStats:
    def __init__(self, name):
        self.name = name
        self.values = []
        self.non_empty_values = []
        self.value_set = set()
        self.count = 0
        self.empty_count = 0
        self.empty_percentage = 0
        self.cardinality = 0

    def add_value(self, value):
        self.count += 1
        if value not in self.value_set:
            self.cardinality += 1
            self.value_set.add(value)
        if value is None:
            self.empty_count += 1
        else:
            self.non_empty_values.append(value)
        self.values.append(value)

    def calculate_stats(self):
        self.empty_percentage = self.empty_count * 100 / self.count if self.count != 0 else 0


class NumberAttributeStats(AttributeStats):
    def __init__(self, name):
        super().__init__(name)
        self.sum = 0
        self.min = None
        self.max = None
        self.quartile_1 = None
        self.quartile_3 = None
        self.average = None
        self.median = None
        self.std_dev = None

    def add_value(self, value):
        super().add_value(value)
        if value is not None:
            self.sum += value
            if self.min is None or value < self.min:
                self.min = value
            if self.max is None or value > self.max:
                self.max = value

    def calculate_stats(self):
        super().calculate_stats()
        self.average = self.sum / self.count if self.count != 0 else 0
        if len(self.non_empty_values) > 0:
            self.quartile_1, self.median, self.quartile_3 = np.quantile(self.non_empty_values, [0.25, 0.5, 0.75])
            self.std_dev = statistics.stdev(self.non_empty_values)


class CategoryAttributeStats(AttributeStats):
    def __init__(self, name):
        super().__init__(name)
        self.counted_values = {}
        self.counted_sorted_values = []
        self.mode = None
        self.mode_count = 0
        self.mode_percentage = 0
        self.mode_2 = None
        self.mode_2_count = 0
        self.mode_2_percentage = 0

    def calculate_stats(self):
        super().calculate_stats()
        for value in self.values:
            self.counted_values[value] = self.counted_values[value] + 1 if value in self.counted_values else 1
        self.counted_sorted_values = [(key, value) for key, value in self.counted_values.items()]
        self.counted_sorted_values.sort(key=lambda pair: pair[1], reverse=True)
        # 1st mode
        if len(self.counted_sorted_values) >= 1:
            self.mode, self.mode_count = self.counted_sorted_values[0]
            self.mode_percentage = self.mode_count * 100 / self.count
            # 2nd mode
            if len(self.counted_sorted_values) >= 2:
                self.mode_2, self.mode_2_count = self.counted_sorted_values[1]
                self.mode_2_percentage = self.mode_2_count * 100 / self.count
