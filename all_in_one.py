import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn import preprocessing
from tabulate import tabulate

data_file_name = "weatherAUS.csv"
normalized_data_file_name = "weatherAUS_normalized.csv"

weather_data = pd.read_csv(data_file_name)
original_weather_data = weather_data

print(weather_data.head())

weather_data["Date"] = pd.to_datetime(weather_data["Date"])
weather_data["Year"] = weather_data["Date"].dt.year
weather_data["Month"] = weather_data["Date"].dt.month
weather_data["Day"] = weather_data["Date"].dt.day
weather_data = weather_data.drop(columns=["Date", "RISK_MM"])

number_columns = [column for column in weather_data.columns if weather_data[column].dtype != "O"]
category_columns = [column for column in weather_data.columns if weather_data[column].dtype == "O"]

# print(f"Number columns: {number_columns}")
# print(tabulate(
#     [[
#         column, len(weather_data[column]), weather_data[column].isnull().sum() * 100 / len(weather_data[column]),
#         weather_data[column].nunique(),
#         weather_data[column].min(), weather_data[column].max(),
#         weather_data[column].quantile(.25), weather_data[column].quantile(.75),
#         weather_data[column].mean(), weather_data[column].quantile(),
#         weather_data[column].std()
#     ] for column in number_columns],
#     ["Attribute", "Row Count", "Missing values, %", "Cardinality",
#      "Min", "Max", "1st quartile", "3rd quartile", "Average", "Median", "Std dev."],
#     tablefmt="psql"
# ))

# pd.DataFrame(
#     [[
#         column, len(weather_data[column]), weather_data[column].isnull().sum() * 100 / len(weather_data[column]),
#         weather_data[column].nunique(),
#         weather_data[column].min(), weather_data[column].max(),
#         weather_data[column].quantile(.25), weather_data[column].quantile(.75),
#         weather_data[column].mean(), weather_data[column].quantile(),
#         weather_data[column].std()
#     ] for column in number_columns],
#     columns=["Attribute", "Row Count", "Missing values, %", "Cardinality",
#              "Min", "Max", "1st quartile", "3rd quartile", "Average", "Median", "Std dev."]
# ).to_csv("weatherAUS_number_columns.csv")

# print(f"Category columns: {category_columns}")
# print(tabulate(
#     [[
#         column, len(weather_data[column]), weather_data[column].isnull().sum() * 100 / len(weather_data[column]),
#         weather_data[column].nunique(),
#         weather_data[column].value_counts().index[0], weather_data[column].value_counts()[0],
#                                            weather_data[column].value_counts()[0] * 100 / len(weather_data[column]),
#         weather_data[column].value_counts().index[1], weather_data[column].value_counts()[1],
#                                            weather_data[column].value_counts()[1] * 100 / len(weather_data[column]),
#     ] for column in category_columns],
#     ["Attribute", "Row Count", "Missing values, %", "Cardinality",
#      "Mode", "Mode count", "Mode, %", "2nd Mode", "2nd Mode count", "2nd Mode, %"],
#     tablefmt="psql"
# ))

# pd.DataFrame(
#     [[
#         column, len(weather_data[column]), weather_data[column].isnull().sum() * 100 / len(weather_data[column]),
#         weather_data[column].nunique(),
#         weather_data[column].value_counts().index[0], weather_data[column].value_counts()[0],
#                                            weather_data[column].value_counts()[0] * 100 / len(weather_data[column]),
#         weather_data[column].value_counts().index[1], weather_data[column].value_counts()[1],
#                                            weather_data[column].value_counts()[1] * 100 / len(weather_data[column]),
#     ] for column in category_columns],
#     columns=["Attribute", "Row Count", "Missing values, %", "Cardinality",
#              "Mode", "Mode count", "Mode, %", "2nd Mode", "2nd Mode count", "2nd Mode, %"]
# ).to_csv("weatherAUS_category_columns.csv")

# number histograms:
# weather_data.hist()

# category histograms:
# category_hist_fig = plt.figure(figsize=(13, 8))
# category_hist_fig.autolayout = True
# category_hist_fig.set_tight_layout(True)
# category_hist_fig_cols = math.ceil(math.sqrt(len(category_columns)))
# category_hist_fig_rows = math.ceil(len(category_columns) / category_hist_fig_cols)
# for i, category in enumerate(category_columns):
#     category_hist_fig.add_subplot(category_hist_fig_rows, category_hist_fig_cols, i + 1)
#     sns.countplot(x=category, data=weather_data)
#     plt.xticks(rotation=90)

# Location with number columns boxplots:
# for number in number_columns:
#     plt.figure()
#     sns.boxplot(x="Location", y=number, data=weather_data)
#     plt.xticks(rotation=90)

# scatter pairplot of number columns
# number_pairplot = sns.pairplot(weather_data[number_columns], corner=True, diag_kind="none")
# # number_pairplot.set("", rotation=90)
# # number_pairplot.set(rotation=0)
# plt.tight_layout()

# correlation = weather_data.corr()
# plt.figure(figsize=(13, 8))
# corr_heatmap_mask = np.zeros_like(correlation)
# corr_heatmap_mask[np.triu_indices_from(corr_heatmap_mask)] = True
# sns.heatmap(correlation, square=True, annot=True, fmt=".2f", linecolor="white", mask=corr_heatmap_mask)
# plt.xticks(rotation=90)

# data normalization

# drop all rows with na
weather_data = weather_data.dropna()

# number column normalization:
mix_max_scaler = preprocessing.MinMaxScaler()
weather_data[number_columns] = mix_max_scaler.fit_transform(weather_data[number_columns].values)

# category column normalization:
weather_data = pd.get_dummies(weather_data, columns=["Location", "WindGustDir", "WindDir9am", "WindDir3pm", "RainToday"])

weather_data.to_csv(normalized_data_file_name, index=False)

plt.show()
